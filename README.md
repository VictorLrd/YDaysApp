# YDaysAPP


[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://gitlab.com/VictorLrd/YDaysApp)

YDaysAPP est une application permettant aux d’avoir un suivi efficace
lors des séances de laboratoire. L’application se destine à chaque membre du groupe d’un
laboratoire.

Dans le cadre des Y-Days organisés sur le campus, YDaysAPP permet aux chefs de pôle d’avoir une trace efficace de chaque séance de laboratoire pour chaque membre du groupe. 

# Fonctionnalités !

  - Gestion des différents rôles ( Chef de pôle, chef de groupe ou bien membre 
  - Suivez en temps direct l'évolution de votre pôle ou bien de votre groupe
  - Suivez l'avancée des projets en postant des reviews à chaque séance ! L'utilisateur à a oublié ? Nous lui envoyons une notification !
  - Vous souhaitez rapidement accéder aux informations d'un groupe ? Utiliser le QRCode !


> Virginie dans ses rêves les plus fous souhaiterais que chaque projet puisse générer un QR
Code qui puisse être scanné à partir de l’application pour atterrir directement sur la page du
projet.


### Technos

YDaysAPP utilisent de nombreuses technos :

* [Ionic](https://ionicframework.com/) - Cross-platform hybrid apps
* [Angular](https://angular.io/) - Develop across all platforms
* [npm](https://www.npmjs.com/) - JavaScript development tools
* [Firebase](https://firebase.google.com/) - Firebase helps mobile app teams succeed


### Installation

Dillinger requires [npm](https://www.npmjs.com/), [Ionic](https://ionicframework.com/) v4 and [Angular](https://angular.io/) to run.

Cloner le repository depuis GitLab :

```sh
$ git clone https://gitlab.com/VictorLrd/YDaysApp
```

Installer les dépendances :

```sh
$ cd YDaysApp
$ npm install
```

Pour lancer l'application sur votre navigateur :

```sh
$ ng serve
```

Pour lancer l'application sur votre smartphone Android :

```sh
$ ionic cordova run android
```

License
----

MIT - Develop By Victor Loredo - Bastien Barbero - Gabriel Poirot

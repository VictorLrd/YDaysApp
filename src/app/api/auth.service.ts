import { Injectable } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth'
import { AngularFirestore } from '@angular/fire/firestore'

import * as firebase from 'firebase/app';

import { Observable } from 'rxjs';
import { users } from '../class/users';

@Injectable()
export class AuthService {
  user: Observable<firebase.User>;

  constructor(private firebaseAuth: AngularFireAuth, public database: AngularFirestore) {
    this.user = firebaseAuth.user;
  }

  signup(email: string, password: string, callback) {
    this.firebaseAuth
      .auth
      .createUserWithEmailAndPassword(email, password)
      .then(value => {
        var userNew = new users();
        this.user.subscribe(user => {
          userNew.id_user = user.uid;
          this.database.collection<users>('users').add(JSON.parse(JSON.stringify(userNew))).then(value => {
            callback(null, value)
          }).catch(err => {
            callback(err, null)
          });
        })
      })
      .catch(err => {
        callback(err, null)
      });
  }

  login(email: string, password: string, callback) {
    this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        callback(null, value)
      })
      .catch(err => {
        callback(err, null)
      });
  }

  logout(callback) {
    this.firebaseAuth
      .auth
      .signOut()
      .then(value => {
        callback(null, value)
      })
      .catch(err => {
        callback(err, null)
      });
  }

  finalSignup(userAdd: users, callback) {
    var userParam = JSON.parse(JSON.stringify(userAdd));
    this.user.subscribe(user => {
      this.database.collection<users>('users', ref => ref.where('id_user', '==', user.uid)).get().subscribe(data => {
        var id;
        data.docs.map(x => id = x.id)
        if (userParam.roleAutre == 'aucun' && userParam.role == 'chefPole') {
          userParam.isInscriptionCompleted = true;
        }
        this.database.doc(`users/${id}`).update({ first_name: userParam.first_name, last_name: userParam.last_name, formation: userParam.formation, group: userParam.group, isInscriptionCompleted: userParam.isInscriptionCompleted, pole: userParam.pole, role: userParam.role, roleAutre: userParam.roleAutre }).then(value => {
          callback(null, "200")
        }).catch(err => {
          callback("400", null)
        });
      });
    })
  }

  finalSignupPole(group, pole, callback) {

    this.user.subscribe(user => {
      this.database.collection<users>('users', ref => ref.where('id_user', '==', user.uid)).get().subscribe(data => {
        var id;
        data.docs.map(x => id = x.id)
        this.database.doc(`users/${id}`).update({ pole: pole, group: group, isInscriptionCompleted: true }).then(value => {
          callback(null, "200")
        }).catch(err => {
          callback("400", null)
        });
      });
    })
  }

  newReviewUser(callback) {

    this.user.subscribe(user => {
      this.database.collection<users>('users', ref => ref.where('id_user', '==', user.uid)).get().subscribe(data => {
        var id;
        data.docs.map(x => id = x.id)
        this.database.doc(`users/${id}`).update({ last_review: new Date().toDateString() }).then(value => {
          callback(null, "200")
        }).catch(err => {
          callback("400", null)
        });
      });
    })
  }

  currentUser() {
    this.user.subscribe(user => {
      return user;
    });
  }

  getUser(callback) {
    this.user.subscribe(user => {
      this.database.collection<users>('users', ref => ref.where('id_user', '==', user.uid)).valueChanges().subscribe(value => {
        callback(null, value)
      });
    });
  }

  async getUsersByGroup(group, callback) {
    this.database.collection<users>('users', ref => ref.where('group', '==', group)).valueChanges().subscribe(value => {
      callback(null, value);
    })
  }

  hasAccessReview(callback) {
    this.getUser((err, res) => {
      if (res[0].role == "chefProject" || res[0].roleAutre == "chefProject") {
        if (err) {
          callback(err, null)
        } else {
          callback(null, true)
        }
      } else {
        callback(null, false)
      }
    })
  }
}
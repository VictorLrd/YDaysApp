import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore'

import { Groupe } from '../class/groupe'
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class GroupeService {

  constructor(public database: AngularFirestore, private authService: AuthService) { }

  getGroupe(callback) {
    this.database.collection<Groupe>('group').valueChanges().subscribe(value => {
      callback(null, value);
    })
  }

  getGroupeByChefPole(callback) {
    this.authService.getUser((err, res) => {
      this.database.collection<Groupe>('group', ref => ref.where('pole', '==', res[0].pole)).valueChanges().subscribe(value => {
        callback(null, value);
      })
    })
  }

  getGroupeByPole(name, callback) {
    this.database.collection<Groupe>('group', ref => ref.where('pole', '==', name)).valueChanges().subscribe(value => {
      callback(null, value);
    })
  }

  getGroupeByPoleSignup(name, callback) {
    this.authService.getUser((err,res) => {
      if(res[0].role == 'membre' || (res[0].role == 'chefPole' && res[0].roleAutre == 'membre' )){
        this.database.collection<Groupe>('group', ref => ref.where('pole', '==', name)).valueChanges().subscribe(value => {
          callback(null, value);
        })
      }else{
        this.database.collection<Groupe>('group', ref => ref.where('pole', '==', name).where('chef', '==', '')).valueChanges().subscribe(value => {
          callback(null, value);
        })
      }
    })
  }

  getGroupeById(id, callback) {
    this.database.collection<Groupe>('group', ref => ref.where('id', '==', id)).valueChanges().subscribe(value => {
      callback(null, value);
    })
  }

  getGroupeByName(name, callback) {
    this.database.collection<Groupe>('group', ref => ref.where('name', '==', name)).valueChanges().subscribe(value => {
      callback(null, value);
    })
  }

  async addGroup(group: Groupe, callback) {  
    this.authService.getUser((err,res) => {
      if(res[0].role == 'chefProject' || res[0].roleAutre == 'chefProject' ){
        group.chef = res[0].first_name + " " + res[0].last_name;
      }
      var groupData = JSON.parse(JSON.stringify(group));
      console.log(groupData);
      this.database.collection<Groupe>('group').add(groupData).then(async value => {
        await this.database.doc(`group/${value.id}`).update({ id: value.id });
        this.getGroupeById(value.id, (err, res) => {
          this.authService.finalSignupPole(res[0].name, res[0].pole, async (err, res) => {
            if (err) {
              callback(err, null);
            } else {
              callback(null, res);
            }
          })
        })
      }).catch(err => {
        callback(err, null)
      });
    })
  }

  async addGroupWhithoutChef(group: Groupe, callback) {
    const groupData = JSON.parse(JSON.stringify(group));
    this.database.collection<Groupe>('group').add(groupData).then(async value => {
      await this.database.doc(`group/${value.id}`).update({ id: value.id });
    }).catch(err => {
      callback(err, null)
    });
  }
}

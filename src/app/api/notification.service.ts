import { Injectable } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { AngularFirestore } from '@angular/fire/firestore';
import { calendar } from '../class/calendar';
import { ToastController } from '@ionic/angular';
import { error } from 'util';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private localNotifications: LocalNotifications,
    public database: AngularFirestore,
    private _toastCtrl: ToastController,
    private authService: AuthService
  ) { }

  private async getCalendar(callback) {
    let yesterday = new Date(new Date().getTime() - 86400000)
    this.database.collection<calendar>('calendar', ref => ref.orderBy('date').startAt(yesterday)).valueChanges().subscribe(data => {
      if (error) {
        callback(error, null)
      } else {
        callback(null, data)
      }
    })
  }

  isDateReview(callback) {
    this.getCalendar((err, res) => {
      if (err) {
        callback(err, null)
      } else {
        let date = new Date(res[0].date.seconds * 1000)
        let today = new Date()
        if (date.getDate() == today.getDate() && date.getMonth() == today.getMonth() && date.getFullYear() == today.getFullYear()) {
          this.authService.getUser((err, res) => {
            const dateUserReview = new Date(res[0].last_review);
            if (dateUserReview.getDate() == today.getDate() && dateUserReview.getMonth() == today.getMonth() && dateUserReview.getFullYear() == today.getFullYear()) {
              callback(null, false)
            }else{
              callback(null, true)
            }
          })
        } else {
          callback(null, false)
        }
      }
    });
  }

  SendNotification() {
    this.getCalendar((err, res) => {
      if (err) {
        (this._toastCtrl.create({
          message: 'Error' + err,
          color: 'danger',
          duration: 2000
        }))
      } else {
        this.localNotifications.schedule({
          text: "Comment s'est passé votre journée ? Veuillez remplir votre avis sur votre journée de labo",
          trigger: { at: new Date(res[1].date.seconds * 1000 + 64800000) },
          led: 'FF0000',
          sound: null
        });
      }
    });
  }
}

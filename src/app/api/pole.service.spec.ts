import { TestBed } from '@angular/core/testing';

import { PoleService } from './pole.service';

describe('PoleService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PoleService = TestBed.get(PoleService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore'
import { Pole } from '../class/pole';

@Injectable({
  providedIn: 'root'
})
export class PoleService {

  constructor(public database: AngularFirestore) { }

  getPole(callback){
    this.database.collection<Pole>('pole').valueChanges().subscribe(value =>{
      callback(null,value);
    })
  }
}

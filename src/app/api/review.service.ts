import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';
import { review } from '../class/review';
import { AuthService } from './auth.service'; 
import * as firebase from 'firebase/app';


@Injectable({
  providedIn: 'root'
})

export class ReviewService {

  constructor(public authService: AuthService, public database: AngularFirestore) {}

  sendReview(review : review, callback) {
    this.authService.user.subscribe(user => {
      review.user = user.uid;
      review.date = new firebase.firestore.Timestamp(Math.round(new Date().getTime()/1000), 0)
      this.authService.getUser((err,res) => {
        var reviewData = JSON.parse(JSON.stringify(review));
        reviewData.group = res[0].group;
        reviewData.pole = res[0].pole;
        reviewData.name = res[0].first_name + " " + res[0].last_name; 
        this.database.collection<review>('review').add(reviewData).then(value =>{
          this.database.doc(`review/${value.id}`).update({id: value.id});
          this.authService.newReviewUser((err,res) =>{
            if(err){
              callback(err,null)
            }else{
              callback(null,value)
            }
          })
        }).catch(err => {
          callback(err,null)
        });
      });
    })
  }

  async getReviews(callback) {
    this.authService.getUser((err,res) => {
      this.database.collection<review>('review', ref => ref.where('pole', '==', res[0].pole)).valueChanges().subscribe(value =>{
        callback(null,value);
      })
    })   
  }

  async getReviewsByGroup(group,callback) {
    this.authService.getUser((err,res) => {
      this.database.collection<review>('review', ref => ref.where('group', '==', group)).valueChanges().subscribe(value =>{
        callback(null,value);
      })
    })   
  }
}

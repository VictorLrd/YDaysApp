import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { task } from '../class/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(public authService: AuthService, public database: AngularFirestore) { }

  async addTask(task : task, callback) {
    this.authService.user.subscribe(user => {
      task.user_id = user.uid;
      this.authService.getUser((err,res) => {
        var taskData = JSON.parse(JSON.stringify(task));
        taskData.group = res[0].group;
        this.database.collection<task>('task').add(taskData).then(value =>{
          this.database.doc(`task/${value.id}`).update({id: value.id});
          callback(null,value)
        }).catch(err => {
          callback(err,null)
        });
      });
    })
  }

  async modifyTask(task : task, callback) {
    this.database.doc(`task/${task.id}`).update({name: task.name, description: task.description, start_date_effective: task.start_date_effective,stop_date_effective: task.stop_date_effective, status : task.status}).then(value => {
      callback(null,"200")
    }).catch(err => {
      callback("400",null)
    });
  }

  async deleteTask(task : task, callback) {
    this.database.doc(`task/${task.id}`).delete().then(value => {
      callback(null,"200")
    }).catch(err => {
      callback("400",null)
    });
  }

  async getTasksByGroup(callback) {
    this.authService.getUser((err,res) => {
      this.database.collection<task>('task', ref => ref.where('group', '==', res[0].group)).valueChanges().subscribe(value =>{
        callback(null,value);
      })
    })
  }

  async getTasksByEtat(etat,callback) {
    this.authService.getUser((err,res) => {
      this.database.collection<task>('task', ref => ref.where('group', '==', res[0].group).where('status', '==', etat)).valueChanges().subscribe(value =>{
        callback(null,value);
      })
    })
  }

  async getTasksById(id,callback) {
    this.database.collection<task>('task', ref => ref.where('id', '==', id)).valueChanges().subscribe(value =>{
      callback(null,value);
    })
  }
  

}

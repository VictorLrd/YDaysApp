import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'pole-select', loadChildren: './pole-select/pole-select.module#PoleSelectPageModule' },
  { path: 'groupe-creation/:type', loadChildren: './groupe-creation/groupe-creation.module#GroupeCreationPageModule' },
  { path: 'review', loadChildren: './review/review.module#ReviewPageModule' },
  { path: 'groupe/:id', loadChildren: './groupe/groupe.module#GroupePageModule' },
  { path: 'task', loadChildren: './task/task.module#TaskPageModule' },
  { path: 'tasks-manager', loadChildren: './tasks-manager/tasks-manager.module#TasksManagerPageModule' },
  { path: 'task-modify/:id', loadChildren: './task-modify/task-modify.module#TaskModifyPageModule' },
  { path: 'qrcode', loadChildren: './qrcode/qrcode.module#QrcodePageModule' },  { path: 'review-list', loadChildren: './review-list/review-list.module#ReviewListPageModule' },




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

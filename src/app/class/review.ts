export class review {
    encountered_difficulties : string = "";
    feeling : string = "";
    group : string = "";
    pole : string ="";
    task_done : string = "";
    user : string = "";
    date : firebase.firestore.Timestamp ;
    id : string = "";
    name: string = "";
}
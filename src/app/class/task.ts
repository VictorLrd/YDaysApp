export class task {
    description : string = "";
    group : string = "";
    name : string = "";
    start_date : string = "";
    start_date_effective : string = "";
    status : string = "1";
    stop_date : string = "";
    stop_date_effective : string = "";
    user_id : string = "";
    id : string = "";
}
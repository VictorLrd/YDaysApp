export class users {
    id_user : string = "";
    first_name : string = "";
    last_name : string = "";
    formation : string = "";
    group : string = "";
    pole : string = "";
    role: string = "";
    roleAutre: string = "aucun";
    isInscriptionCompleted: boolean = false;
    last_review : string = ""
}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GroupeCreationPage } from './groupe-creation.page';
import { ShareModule } from '../shared/share.module';

const routes: Routes = [
  {
    path: '',
    component: GroupeCreationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ShareModule,
  ],
  declarations: [GroupeCreationPage]
})
export class GroupeCreationPageModule {}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupeCreationPage } from './groupe-creation.page';

describe('GroupeCreationPage', () => {
  let component: GroupeCreationPage;
  let fixture: ComponentFixture<GroupeCreationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupeCreationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupeCreationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

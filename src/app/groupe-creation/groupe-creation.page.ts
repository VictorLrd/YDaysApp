import { Component, OnInit } from '@angular/core';
import { Groupe } from '../class/groupe';
import { GroupeService } from '../api/groupe.service';
import { ToastController } from '@ionic/angular';
import { PoleService } from '../api/pole.service';
import { AuthService } from '../api/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-groupe-creation',
  templateUrl: './groupe-creation.page.html',
  styleUrls: ['./groupe-creation.page.scss'],
})
export class GroupeCreationPage implements OnInit {

  group : Groupe;
  poles: any[] = [];
  isInscription : boolean

  constructor(private router : Router, private route : ActivatedRoute, private groupService : GroupeService, private _toastCtrl: ToastController, private poleService : PoleService, private authService : AuthService) { }

  ngOnInit() {
    this.group = new Groupe();
    this.getPole();
    this.isInscription = this.route.snapshot.params['type'];
  }

  getPole(){
    this.poleService.getPole((err,res) => {
      this.poles = res;
    });
  }

  
  onChangePole(event){
    this.group.pole = event.detail.value;
  }

  addGroup(){
    if(this.isInscription){
      this.groupService.addGroup(this.group,async (err,res) =>{
        if(err){
          (await this._toastCtrl.create({
            message: 'Error' + err,
            color: 'danger',
            duration: 2000
          })).present()
        }else{
            (await this._toastCtrl.create({
              message: 'Bienvenue !',
              color: 'success',
              duration: 2000
            })).present();
            this.router.navigate(['home']);
        }
      })
    }else{
      this.groupService.addGroupWhithoutChef(this.group,async (err,res) =>{
        if(err){
          (await this._toastCtrl.create({
            message: 'Error' + err,
            color: 'danger',
            duration: 2000
          })).present()
        }else{
            (await this._toastCtrl.create({
              message: 'Bien ajoutée !',
              color: 'success',
              duration: 2000
            })).present();
            this.router.navigate(['home']);
        }
      })
    }
   
  }

}

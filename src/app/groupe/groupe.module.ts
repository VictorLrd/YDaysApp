import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GroupePage } from './groupe.page';
import { ShareModule } from '../shared/share.module';

const routes: Routes = [
  {
    path: '',
    component: GroupePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ShareModule
  ],
  declarations: [GroupePage]
})
export class GroupePageModule {}

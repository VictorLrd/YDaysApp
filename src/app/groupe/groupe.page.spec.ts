import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupePage } from './groupe.page';

describe('GroupePage', () => {
  let component: GroupePage;
  let fixture: ComponentFixture<GroupePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

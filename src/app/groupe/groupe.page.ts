import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Groupe } from '../class/groupe';
import { GroupeService } from '../api/groupe.service';
import { AuthService } from '../api/auth.service';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


@Component({
  selector: 'app-groupe',
  templateUrl: './groupe.page.html',
  styleUrls: ['./groupe.page.scss'],
})
export class GroupePage implements OnInit {

  groupe : Groupe = new Groupe()
  membres: any[] = [];
  encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;

  constructor(private route : ActivatedRoute,
    private groupeService : GroupeService, 
    private userService : AuthService,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private barcodeScanner: BarcodeScanner) { }

  ngOnInit() {
    this.groupeService.getGroupeById(this.route.snapshot.params['id'],(err,res) => {
      this.groupe = res[0]
      this.getUsersByGroup();
    })

  }

  getUsersByGroup(){
    this.userService.getUsersByGroup(this.groupe.name,(err,res) =>{
      this.membres = res;
    })
  }

  encodedQRCode() {
    const url = "/groupe/"+this.route.snapshot.params['id']
    this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE, url).then((encodedData) => {
      this.encodeData = encodedData;
    }, (err) => {
    });
  }

}

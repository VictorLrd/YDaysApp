import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AngularFireStorageModule } from '@angular/fire/storage'

import { HomePage } from './home.page';
import { ShareModule } from '../shared/share.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    AngularFireStorageModule,
    ShareModule
  ],
  declarations: [HomePage]
})
export class HomePageModule {}

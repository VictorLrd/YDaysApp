import { Component } from '@angular/core';
import { AngularFireStorage} from '@angular/fire/storage'
import { ToastController } from '@ionic/angular';
import { AuthService } from '../api/auth.service';
import { Router } from '@angular/router';
import { GroupeService } from '../api/groupe.service';
import { NotificationService } from '../api/notification.service';
import { PoleService } from '../api/pole.service';
import { users } from '../class/users';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  img : string;

  items: any[] = [];
  poles: any[] = [];
  groupeSelect : string;
  poleSelect : string;
  role : string;
  notif : boolean;
  groupe : string;
  spinner : boolean = false;

  constructor(
    private _storage : AngularFireStorage,
    private _toastCtrl: ToastController,
    public authService: AuthService,
    public groupeService : GroupeService,
    public poleService: PoleService,
    public notificationService: NotificationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getPole();
    this._storage.ref('loredo.jpg').getDownloadURL().subscribe(link => {
      this.img = link;
    })

    this.authService.getUser((err,res)=>{
      this.role = res[0].role;
      this.groupe =  res[0].group;
    })

    this.notificationService.isDateReview((err,res)=>{
      this.notif = res;
    })
  }

  logout(){
    this.authService.logout(async (err,res) => {
      if(err){
        (await this._toastCtrl.create({
          message: 'Error' + err,
          color: 'danger',
          duration: 2000
        })).present()
      }else{
        (await this._toastCtrl.create({
          message: 'Déconnectée !',
          color: 'success',
          duration: 2000
        })).present();
        this.router.navigate(['login']);
      }
    })
  }

  selectGroup(){
    this.groupeService.getGroupe((err,res) => {
      this.items = res
    });
  }

  selectGroupById(name){
    this.spinner = true;
    this.groupeService.getGroupeByPole(name,(err,res) => {
      this.items = res
      this.spinner = false;
    });
  }

  getPole(){
    this.poleService.getPole((err,res) => {
      this.poles = res;
    });
  }

  onChangePole(event){
    this.selectGroupById(event.detail.value);
    this.poleSelect = event.detail.value;
  }

  accessGroup(){
    this.authService.getUser((err,res) =>{
      this.groupeService.getGroupeByName(res[0].group,(err,res)=>{
        this.router.navigate(['groupe/'+res[0].id])
      })
    })
  }

}

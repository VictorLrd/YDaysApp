import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ToastController } from '@ionic/angular'
import { AuthService } from '../api/auth.service'
import { Router } from '@angular/router'


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage{

  email: string;
  password: string;

  constructor(
    private _toastCtrl: ToastController,
    public authService: AuthService,
    private router: Router
  ) { }


  ionViewDidEnter() {
    this.authService.user.subscribe(data => {
      if (data) {
        this.authService.getUser((err,res) =>{
          if(res[0].isInscriptionCompleted){
            this.router.navigate(['home']);
          }else{
            this.router.navigate(['signup'])
          }
        })
      }
    })
  }


  login() {
    this.authService.login(this.email, this.password, async (err, res) => {
      if (err) {
        (await this._toastCtrl.create({
          message: 'Error' + err,
          color: 'danger',
          duration: 2000
        })).present()
      } else {
        (await this._toastCtrl.create({
          message: 'Connectée !',
          color: 'success',
          duration: 2000
        })).present();
      }
    })
    this.email = this.password = '';
  }

  logout() {
    this.authService.logout(async (err, res) => {
      if (err) {
        (await this._toastCtrl.create({
          message: 'Error' + err,
          color: 'danger',
          duration: 2000
        })).present()
      } else {
        (await this._toastCtrl.create({
          message: 'Déconnectée !',
          color: 'success',
          duration: 2000
        })).present();
      }
    })
  }

  signup() {
    this.authService.signup(this.email, this.password, async (err, res) => {
      if (err) {
        (await this._toastCtrl.create({
          message: 'Error' + err,
          color: 'danger',
          duration: 2000
        })).present()
      } else {
        (await this._toastCtrl.create({
          message: 'Bienvenue !',
          color: 'success',
          duration: 2000
        })).present();

      }
    })
    this.email = this.password = '';
  }

}

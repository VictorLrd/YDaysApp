import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoleSelectPage } from './pole-select.page';

describe('PoleSelectPage', () => {
  let component: PoleSelectPage;
  let fixture: ComponentFixture<PoleSelectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoleSelectPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoleSelectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { GroupeService } from '../api/groupe.service';
import {AuthService} from '../api/auth.service'
import { PoleService } from '../api/pole.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-pole-select',
  templateUrl: './pole-select.page.html',
  styleUrls: ['./pole-select.page.scss'],
})
export class PoleSelectPage implements OnInit {

  items: any[] = [];
  poles: any[] = [];
  groupeSelect : string
  poleSelect : string

  constructor(
      public groupeService : GroupeService,
      public authService: AuthService,
      public poleService: PoleService,
      private router: Router
  ) {
  }

  ngOnInit() {
    this.getPole();
  }

  ionViewDidEnter(){
    this.authService.getUser((err,res) => {
      if(res[0]){ 
        if(res[0].isInscriptionCompleted){
          this.router.navigate(['home']);
        }
      }
    })
  }

  selectGroup(){
    this.groupeService.getGroupe((err,res) => {
      this.items = res
    });
  }

  selectGroupById(name){
    this.groupeService.getGroupeByPoleSignup(name,(err,res) => {
      console.log(res)
      this.items = res
    });
  }

  getPole(){
    this.poleService.getPole((err,res) => {
      this.poles = res;
    });
  }

  onChangePole(event){
    this.selectGroupById(event.detail.value);
    this.poleSelect = event.detail.value;
  }

  finalSignupPole(){
    this.authService.finalSignupPole(this.groupeSelect,this.poleSelect,(err,res) => {
      if(res == "200"){
        this.router.navigate(['home']);
      }
    })
  }


}


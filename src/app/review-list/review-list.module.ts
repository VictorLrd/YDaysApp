import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReviewListPage } from './review-list.page';
import { ShareModule } from '../shared/share.module';

const routes: Routes = [
  {
    path: '',
    component: ReviewListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ShareModule
  ],
  declarations: [ReviewListPage]
})
export class ReviewListPageModule {}

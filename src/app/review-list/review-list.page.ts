import { Component, OnInit } from '@angular/core';
import { ReviewService } from '../api/review.service';
import { GroupeService } from '../api/groupe.service';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.page.html',
  styleUrls: ['./review-list.page.scss'],
})
export class ReviewListPage implements OnInit {

  reviews : any[]
  groupes : any[]
  groupeSelect : string

  constructor(private reviewService : ReviewService, private groupeService : GroupeService) { }

  ngOnInit() {
    this.reviewService.getReviews((err,res) => {
      this.reviews = res;
      console.log(this.reviews)
    })
    this.groupeService.getGroupeByChefPole((err,res) => {
      this.groupes = res;
    })
  }

  onChangeGroup(event){
    this.selectReviewByGroup(event.detail.value);
    this.groupeSelect = event.detail.value;
  }

  selectReviewByGroup(group){
    this.reviewService.getReviewsByGroup(group,(err,res)=>{
      this.reviews = res;
    })
  }

  date(item){
    let date = new Date(item.date.seconds * 1000).toLocaleDateString();
    return date;
  }

}

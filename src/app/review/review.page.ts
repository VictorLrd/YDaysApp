import { Component, OnInit } from '@angular/core';
import { ReviewService } from '../api/review.service';
import { review } from '../class/review';
import { ToastController } from '@ionic/angular'
import { Router } from '@angular/router'
import { LoadingController } from '@ionic/angular';
import { NotificationService } from '../api/notification.service'


@Component({
  selector: 'app-review',
  templateUrl: './review.page.html',
  styleUrls: ['./review.page.scss'],
})
export class ReviewPage implements OnInit {

  review: review;
  isSend: boolean = false;

  constructor(
    public reviewService: ReviewService,
    private _toastCtrl: ToastController,
    private router: Router,
    public loadingController: LoadingController,
    private _notifactionService: NotificationService
  ) { }

  ngOnInit() {
    this.review = new review();
    this.isSend = false;
  }


  async send() {
    this.isSend = true;
    if (this.review.feeling != "" && this.review.encountered_difficulties != "" && this.review.task_done != "") {
      this.reviewService.sendReview(this.review, async (err, res) => {
        if (err) {
          (await this._toastCtrl.create({
            message: 'Error' + err,
            color: 'danger',
            duration: 2000
          })).present()
        } else {
          this._notifactionService.SendNotification();
          this.router.navigate(['home']);
        }
      })
    } else {
      (await this._toastCtrl.create({
        message: 'Veuillez remplir tout les champs',
        color: 'danger',
        duration: 2000
      })).present()
      this.isSend = false;
    }

  }

}

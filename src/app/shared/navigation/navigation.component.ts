import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../api/auth.service';
import { ToastController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { GroupeService } from 'src/app/api/groupe.service';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NotificationService } from '../../api/notification.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {

  img : string;
  barcodeScannerOptions: BarcodeScannerOptions;
  notif : boolean;
  role : string;
  roleAutre : String;
  
  constructor(
    public notificationService: NotificationService,
    private _toastCtrl: ToastController,
    public authService: AuthService,
    private router: Router,
    public groupeService: GroupeService,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private barcodeScanner: BarcodeScanner,
    private platform: Platform,
  ) {
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
   }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    
    this.notificationService.isDateReview((err,res)=>{
      this.notif = res;
    })

    this.authService.getUser((err,res)=>{
      this.role = res[0].role;
      this.roleAutre = res[0].roleAutre;
    })
  }

  logout(){
    this.authService.logout(async (err,res) => {
      if(err){
        (await this._toastCtrl.create({
          message: 'Error' + err,
          color: 'danger',
          duration: 2000
        })).present()
      }else{
        (await this._toastCtrl.create({
          message: 'Déconnectée !',
          color: 'success',
          duration: 2000
        })).present();
        this.router.navigate(['login']);
      }
    })
  }

  accessGroup(){
    this.authService.getUser((err,res) =>{
      this.groupeService.getGroupeByName(res[0].group,(err,res)=>{
        this.router.navigate(['groupe/'+res[0].id])
      })
    })
  }

  scanCode() {
    this.barcodeScanner.scan().then( async barcodeData => {
      if(this.router.isActive(barcodeData.text,true)){
        this.router.navigate([barcodeData.text]);
      }else{
        await this._toastCtrl.create({
          message: 'Mauvais QrCode !',
          color: 'success',
          duration: 2000
        });
        this.router.navigate(['home']);
      }
    }).catch(err => {
      console.log('Error', err);
    });
  }

}

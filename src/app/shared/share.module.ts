import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './navigation/navigation.component';

import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [
    NavigationComponent
  ],
  exports : [
    NavigationComponent
  ],
  imports: [
    IonicModule.forRoot(), 
    CommonModule
  ]
})
export class ShareModule { }

import { Component, OnInit } from '@angular/core';
import { users } from '../class/users'
import { ToastController } from '@ionic/angular'
import { AuthService } from '../api/auth.service'
import { Router } from '@angular/router'
import { PoleService } from '../api/pole.service';
import { NotificationService } from '../api/notification.service'


@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  user: users;

  constructor(
    public authService: AuthService,
    private _toastCtrl: ToastController,
    private router: Router,
    public poleService: PoleService,
    private _notifactionService: NotificationService
  ) { }

  ngOnInit() {
    this.getPole();
    this.user = new users();
    this.authService.user.subscribe(user => {
      this.user.id_user = user.uid
    })
  }

  ionViewDidEnter() {
    this.authService.getUser((err, res) => {
      if (res[0]) {
        if (res[0].isInscriptionCompleted) {
          this.router.navigate(['home']);
        }
      }
    })
  }



  finalSignup() {
    if (this.user.role === 'membre' || this.user.role === 'chefProject') {
      this.user.roleAutre = 'aucun';
    }
    this.authService.finalSignup(this.user, async (err, res) => {
      if (err) {
        (await this._toastCtrl.create({
          message: 'Error' + err,
          color: 'danger',
          duration: 2000
        })).present()
      } else {
        if (this.user.roleAutre == 'aucun' && this.user.role == 'chefPole') {
          this._notifactionService.SendNotification();
          this.router.navigate(['home']);
        } else {
          this.router.navigate(['pole-select']);
        }
      }
    })
  }

  finalSignupCreationGroup() {
    if (this.user.role === 'membre' || this.user.role === 'chefProject') {
      this.user.roleAutre = 'aucun';
    }
    this.authService.finalSignup(this.user, async (err, res) => {
      if (err) {
        (await this._toastCtrl.create({
          message: 'Error' + err,
          color: 'danger',
          duration: 2000
        })).present()
      } else {
        this.router.navigate(['groupe-creation/true']);
      }
    })
  }

  poles: any[] = [];

  getPole() {
    this.poleService.getPole((err, res) => {
      this.poles = res;
    });
  }

}

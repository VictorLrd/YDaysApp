import { Component, OnInit } from '@angular/core';
import { task } from '../class/task';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskService } from '../api/task.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-task-modify',
  templateUrl: './task-modify.page.html',
  styleUrls: ['./task-modify.page.scss'],
})
export class TaskModifyPage implements OnInit {

  task : task

  constructor(private route : ActivatedRoute, private taskService : TaskService,private _toastCtrl: ToastController, private router: Router) { }

  ngOnInit() {
    this.taskService.getTasksById(this.route.snapshot.params['id'],(err,res) => {
      this.task = res[0]
    })
  }

  modifyTask(){
    this.taskService.modifyTask(this.task, async (err,res) => {
      if(res === "200"){
        (await this._toastCtrl.create({
          message: 'Bien modifié !',
          color: 'success',
          duration: 3000
        })).present()
        this.router.navigate(['tasks-manager']);
      }else{
        (await this._toastCtrl.create({
          message: 'Erreur:' + err,
          color: 'danger',
          duration: 2000
        })).present();
      }
    })
  }

  deleteTask(){
    this.taskService.deleteTask(this.task, async (err,res) => {
      if(res === "200"){
        (await this._toastCtrl.create({
          message: 'Bien supprimé !',
          color: 'success',
          duration: 3000
        })).present()
        this.router.navigate(['tasks-manager']);
      }else{
        (await this._toastCtrl.create({
          message: 'Erreur:' + err,
          color: 'danger',
          duration: 2000
        })).present();
      }
    })
  }


}

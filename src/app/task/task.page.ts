import { Component, OnInit } from '@angular/core';
import { task } from '../class/task';
import { TaskService } from '../api/task.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {

  task : task

  constructor(private taskService : TaskService) { }

  ngOnInit() {
    
    this.task = new task();
  }

  addTask(){
    this.taskService.addTask(this.task,(err,res) => {
      console.log("Bien ajouté", res)
    })
  }

}

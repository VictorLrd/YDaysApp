import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TasksManagerPage } from './tasks-manager.page';
import { ShareModule } from '../shared/share.module';

const routes: Routes = [
  {
    path: '',
    component: TasksManagerPage
  }
];

@NgModule({
  imports: [
    ShareModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TasksManagerPage]
})
export class TasksManagerPageModule {}

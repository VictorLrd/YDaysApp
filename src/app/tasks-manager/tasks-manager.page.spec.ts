import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TasksManagerPage } from './tasks-manager.page';

describe('TasksManagerPage', () => {
  let component: TasksManagerPage;
  let fixture: ComponentFixture<TasksManagerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TasksManagerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksManagerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

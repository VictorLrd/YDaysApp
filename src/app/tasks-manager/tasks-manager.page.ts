import { Component, OnInit } from '@angular/core';
import { TaskService } from '../api/task.service';

@Component({
  selector: 'app-tasks-manager',
  templateUrl: './tasks-manager.page.html',
  styleUrls: ['./tasks-manager.page.scss'],
})
export class TasksManagerPage implements OnInit {

  tasks: any[] = [];

  role: any;
  constructor(private taskService : TaskService) { }

  ngOnInit() {
    this.getTaskGroup();
  }

  getTaskGroup(){
    this.taskService.getTasksByGroup((err,res) =>{
      this.tasks = res;
    });
  }

  getTaskGroupByEtat(etat){
    this.taskService.getTasksByEtat(etat,(err,res) =>{
      this.tasks = res;
    });
  }

  changeFiltre(event){
    if(event.detail.value === "0"){
      this.getTaskGroup()
    }else{
      this.getTaskGroupByEtat(event.detail.value)
    }
  }

}
